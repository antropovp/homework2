package com.epam.practice3.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceTemplate {

    public String replace(String input, String key, String value) {
        Pattern pkey = Pattern.compile("[\\w'$]+");
        Matcher mkey = pkey.matcher(key);

        Pattern pvalue = Pattern.compile("[\\w'-]+");
        Matcher mvalue = pvalue.matcher(value);

        while ((mkey.find()) && (mvalue.find())) {
            input = input.replace(key.substring(mkey.start(), mkey.end()), value.substring(mvalue.start(), mvalue.end()));
        }

        return input;
    }

}
