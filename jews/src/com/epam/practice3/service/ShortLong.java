package com.epam.practice3.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShortLong {

    public String shortest(String input){
        String word = "";
        int min = input.length();

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(input);
        String string = "";

        while (m.find()) {
            string = input.substring(m.start(), m.end());
            if (string.length() < min) {
                word = string;
                min = string.length();
            }
        }

        return word;
    }

    public String longest(String input){
        String word = "";
        int max = 0;

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(input);
        String string = "";

        while (m.find()) {
            string = input.substring(m.start(), m.end());
            if (string.length() > max) {
                word = string;
                max = string.length();
            }
        }

        return word;
    }
}
