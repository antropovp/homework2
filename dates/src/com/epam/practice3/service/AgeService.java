package com.epam.practice3.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AgeService {

    public Date getAge(GregorianCalendar birth) {

        SimpleDateFormat fmt = new SimpleDateFormat();
        GregorianCalendar now = new GregorianCalendar();

        GregorianCalendar resultDate = new GregorianCalendar(now.get(Calendar.YEAR) - birth.get(Calendar.YEAR),
                now.get(Calendar.MONTH) - birth.get(Calendar.MONTH),
                now.get(Calendar.DAy) - birth.get(Calendar.DAY_OF_MONTH),
                now.get(Calendar.HOUR_OF_DAY) - birth.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE) - birth.get(Calendar.MINUTE),
                now.get(Calendar.SECOND) - birth.get(Calendar.SECOND));


        return resultDate.getTime();
    }

}
