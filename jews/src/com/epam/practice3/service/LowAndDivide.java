package com.epam.practice3.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LowAndDivide {

    public String low(String input) {
        String string = input.toLowerCase();
        return string;
    }

    public String divide(String input) {
        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(input);
        String string = "";

        while (m.find()) {
            string += input.substring(m.start(), m.end()) + "\n";
        }

        return string;
    }

}
