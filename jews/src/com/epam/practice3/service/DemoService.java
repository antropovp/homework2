package com.epam.practice3.service;

public class DemoService {

    public void demo() {
        LowAndDivide lad = new LowAndDivide();
        ShortLong sl = new ShortLong();
        ReplaceTemplate rt = new ReplaceTemplate();

        String text = "jimm, jsa wf'we  askjffnwej sdss sd.";

        String template = "Hey $name wussup dawg. gimme my $thing, i need it badly, where are u $slur. Your nigga, $thisName";
        String templateKey = "$name $thisName $slur $thing";
        String templateValue = "Tyrone DeShawn asshole toilet-paper";

        System.out.println("Text: " + text);
        System.out.println();

        System.out.println("low and divided:\n" + lad.divide(lad.low(text)));

        System.out.println("shortest: " + sl.shortest(text));
        System.out.println("longest: " + sl.longest(text));
        System.out.println();

        System.out.println("Template: " + template);
        template = rt.replace(template, templateKey, templateValue);
        System.out.println("Result: " + template);
    }

}
